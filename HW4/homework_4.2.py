"""Дана довільна строка. Напишіть код, який знайде в ній і виведе на екран кількість слів, які містять дві голосні літери підряд."""

word_counter = 0
old_letter = " "

word_string = input("Give a sentence/word/phrase, please: ")

list_word_string = word_string.split()

print("Here you can see the vowels:")
vowels = ["a", "o", "u", "e", "i"]
print(vowels)
print("Here you can see the words that have 2 or more vowels in a row:")

for word in list_word_string:
    old_letter = " "
    for letter in word:
        if letter in vowels and old_letter in vowels:
            word_counter += 1
            print(word)
            break
        else:
            old_letter = letter
print(f'The number of words that includes vowels is {word_counter}')
