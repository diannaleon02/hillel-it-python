import library

assert library.is_string_capitalized('My name is David') is False
assert library.is_string_capitalized('I love playing') is True
assert library.is_string_capitalized('') is True
assert library.is_string_capitalized('565656') is True
assert library.is_string_capitalized(' ') is True
assert library.is_string_capitalized(' W') is True

odd_numbers = [2, 4, 10, 14, -8, -24, "6", "-8", "-22", "0"]

for odd_number in odd_numbers:
    assert library.is_odd(odd_number) is True

even_numbers = [1, -1, 5, -5, 2.2, -1.2, "1", "-1", "-2.1", "-1.1"]

for even_number in even_numbers:
    assert library.is_odd(even_number) is False
