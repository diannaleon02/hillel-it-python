def is_string_capitalized(input_string: str):
    """
    Function check if the given string is capitalised.
    Upper letters inside of string return False.

    Args:
        input_string (str): string to check on capitalised

    Returns:
        bool: True if input_string is capitalised, or False in other case
    """
    if (not isinstance(input_string, str)):
        raise TypeError
    input_string = ' '.join(input_string.split())
    if (len(input_string) == 0):
        return True
    if (input_string.isdigit()):
        return True
    if ((input_string[0].isupper() and not (input_string[0].isdigit()))
            or (input_string[0].isdigit())):
        for char in input_string[1:]:
            if (input_string.isdigit()):
                continue
            if (char.isupper()):
                return False
        return True
    else:
        return False


def is_odd(number: int | float | str):
    """
        This function check number on odd and even.

        Args:
            number (int | float | str): number to check if it odd or even

        Returns:
            bool: if number is odd - True, if not False
    """

    if (not isinstance(number, int | float | str)):
        raise TypeError
    try:
        number = float(number)
    except ValueError:
        raise TypeError

    if (number % 2) == 0:
        return True
    else:
        return False
