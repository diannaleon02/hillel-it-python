import random
import time


def func_time(func):
    def wrapper(*args, **qwargs):
        st = time.time()
        result = func(*args, **qwargs)
        et = time.time()
        elapsed_time = et - st
        print('Execution time:', elapsed_time, 'seconds')
        return result
    return wrapper


def request_input():
    input_data = input("Give me a number between 1 and 100: ")
    while True:
        if input_data.isdigit():
            input_data = int(input_data)
            if (input_data <= 100) and (input_data >= 1):
                return input_data
            else:
                input_data = input("Give me a number between 1 and 100, not more than 100 and not less than 1 !!!: ")
        else:
            input_data = input("Give me a number between 1 and 100, it must be a number !!!: ")


def check_number(user_number, target_number):
    difference = (abs(target_number - user_number))
    if difference == 0:
        print("Congratulation!")
        return True
    else:
        check_diff(difference)
        return False

def check_diff(difference):
    if (difference) > 10:
        print("Cold!")
    elif 5 <= (difference) <= 10:
        print("Warm!")
    elif 1 <= (difference) < 5:
        print("Hot!")

def request_continue():
    while True:
        input_data = input("Play more? Answer \"Yes\" or \"No\":  ")
        input_data = input_data.lower()
        if input_data == "yes":
            return False
        elif input_data == "no":
            return True


@func_time
def game():
    while True:
        target_number = random.randint(1, 100)

        while True:
            user_number = request_input()
            guess_or_not = check_number(user_number, target_number)
            if guess_or_not:
                break

        play_agan = request_continue()
        if play_agan:
            break
