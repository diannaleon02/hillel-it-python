"""Доопрацюйте класс Triangle з попередньої домашки наступним чином:
обʼєкти классу Triangle можна порівнювати між собою (==, !=, >, >=, <, <=) за площею.
перетворення обʼєкту классу Triangle на стрінг показує координати його вершин у форматі x1, y1 -- x2, y2 -- x3, y3"""

import copy

class Point:
    _x = None
    _y = None

    def __init__(self, dot_x=0, dot_y=0):
        if (isinstance(dot_x, (int, float))
                and isinstance(dot_y, (int, float))):
            self._x = dot_x
            self._y = dot_y
        else:
            raise TypeError

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, dot_x):
        if (isinstance(dot_x, (int, float))):
            self._x = dot_x
        else:
            raise TypeError

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, dot_y):
        if (isinstance(dot_y, (int, float))):
            self._y = dot_y
        else:
            raise TypeError

class Line:
    _begin = None
    _end = None

    def __init__(self, begin_point, end_point):
        self.begin = begin_point
        self.end = end_point

    @property
    def length(self):
        cat1 = self.begin.x - self.end.x
        cat2 = self.begin.y - self.end.y

        res = (cat1 ** 2 + cat2 ** 2) ** 0.5

        return res

    @property
    def begin(self):
        return self._begin

    @begin.setter
    def begin(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self._begin = value

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self._end = value

class Triangle:
    _tops = []
    _sides = []

    def __init__(self, top_points):
        if(isinstance(top_points, list)):
            for top in top_points:
                if (not isinstance(top, Point)):
                    raise TypeError
        else:
            raise TypeError

        self._tops = copy.deepcopy(top_points)

        self._sides = [Line(self._tops[0], self._tops[1])]
        self._sides.append(Line(self._tops[1], self._tops[2]))
        self._sides.append(Line(self._tops[2], self._tops[0]))


    @property
    def tops(self):
        return self._tops

    @tops.setter
    def tops(self, top_points):
            for top in top_points:
                if(not isinstance(top, Point)):
                    raise TypeError
            self._tops = copy.deepcopy(top_points)

    @property
    def sides(self):
        return self._sides

    @property
    def area(self):
        half_perimitr = 0
        for side in self.sides:
            half_perimitr += side.length

        half_perimitr /= 2

        area = (half_perimitr * (half_perimitr - self.sides[0].length) * (half_perimitr - self.sides[1].length) * (half_perimitr - self.sides[2].length)) ** 0.5
        return area

    def __lt__(self, other):
        if (self.area < other.area):
            return True
        else:
            return False

    def __le__(self, other):
        if (self.area <= other.area):
            return True
        else:
            return False

    def __eq__(self, other):
        if (self.area == other.area):
            return True
        else:
            return False

    def __ne__(self, other):
        if (self.area != other.area):
            return True
        else:
            return False

    def __ge__(self, other):
        if (self.area >= other.area):
            return True
        else:
            return False

    def __gt__(self, other):
        if (self.area > other.area):
            return True
        else:
            return False

    def __str__(self):
        points = f'{self._tops[0].x}, {self._tops[0].y} -- {self._tops[1].x}, {self._tops[1].y} -- {self._tops[2].x}, {self._tops[2].y}'
        return points


point_1 = Point(0, 0)
point_2 = Point(2, 0)
point_3 = Point(0, 2)

point_4 = Point(0, 0)
point_5 = Point(1, 0)
point_6 = Point(0, 1)

triangle_2 = Triangle([point_1, point_2, point_3])

triangle = Triangle([point_4, point_5, point_6])

print(str(triangle))
print(triangle.area)

print(str(triangle_2))
print(triangle_2.area)

assert (triangle > triangle_2) is False
assert (triangle < triangle_2) is True
assert (triangle == triangle_2) is False
assert (triangle != triangle_2) is True




