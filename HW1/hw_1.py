#Задача: Створіть дві змінні first=10, second=30.
#Виведіть на екран результат математичної взаємодії (+, -, *, / и тд.) для цих чисел.

int_1= 31
int_2= 10

result = int_1 + int_2  #addition
print(result)

result = int_1 - int_2  #substraction
print(result)

result = int_1 * int_2  #multiplication
print(result)

result = int_1 / int_2  #division
print(result)

result = int_1 // int_2  #integer division
print(result)

result = int_1 ** int_2  #exponent
print(result)

result = int_1 % int_2   #modulus/remainder
print(result)


#Задача: Створіть змінну і запишіть в неї результат порівняння (<, > , ==, !=) чисел з завдання 1.
#Виведіть на екран результат кожного порівняння.

bool_result = int_1 > int_2
print(bool_result)
print(type(bool_result)) #demonstrates the type of the value
bool_result = int_1 < int_2
print(bool_result)
bool_result = int_1 == int_2
print(bool_result)
bool_result = int_1 != int_2
print(bool_result)
bool_result = int_1 >= int_2
print(bool_result)
bool_result = int_1 <= int_2
print(bool_result)

#boolean values are demonstrated along with math operations

bool_result = (int_2 / 5) == ((int_1 - 1) - (int_2 *2) -(int_2 - 2))
print(bool_result)

#Задача: Створіть змінну - результат конкатенації (складання) строк
#str1="Hello " и str2="world". Виведіть на екран.

phrase_1 = "- Was möchten Sie bei unserer Bäckerei bestellen?"

phrase_2 = "- Ich möchte Heiße Schokolade!"

phrase_sum = phrase_1 + "\n" + phrase_2

print(phrase_sum)