from pprint import pprint

# 1. Є дікт, невідомого наповнення. В дікті присутні ключі, значенням для яких є дікти невідомого наповнення в яких можуть бути аналогічні вкладені дікти.
# Напишіть функцію, яка дістане всі ключі зі значеннями не-діктами з усіх рівнів вкладення, помістить на один рівень в окремий дікт і поверне цей дікт.

dictionary = {
    "dict_1":
        {
            "1": "panda_1",
            "2": "panda_2",
            "88": "panda_3",
            "67": "panda_4",
            "dict_2":
                {
                    "11": "Maria",
                    "22": "Anna",
                    "55": "Georg",
                    "145": "Eduard",
                    "90": "Lisa",
                    "dict_3":
                        {
                            "222": "programmer",
                            "345": "doctor",
                            "900": "astronaut"
                        }
                }
        }
}

def func_dict(elements):
    """
       "func_dict" function converts a multi-level dict to a single-level one.
       There is no limits to number of dict's levels.
       The keys and values that are not dict are saved.

       :param: elements: dict, which is the base for single-level dict converting
       :type: elements: dict

       :return: single-level dict
       :rtype: dict
   """
    one_level_dict = {}
    for elem in elements.items():
        if isinstance(elem[1], dict):
            one_level_dict.update(func_dict(elem[1]))
        else:
            one_level_dict[elem[0]] = elem[1]
    return one_level_dict

help(func_dict)   #докстрінг для цієї функці виведений

result = func_dict(dictionary)

pprint(result)


