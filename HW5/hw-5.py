import requests
from pprint import pprint

print('----------------------------------------------------------------Task 1-------------------------------------------------------------------')

"""завдання 1
урл http://api.open-notify.org/astros.json
вивести список всіх астронавтів, що перебувають в даний момент на орбіті (дані не фейкові, оновлюються в режимі реального часу)"""

url = f'http://api.open-notify.org/astros.json'

response = requests.get(url)

response_json = response.json()

people_list = response_json['people']

for people in people_list:
    pprint(f"{people['name']} is now on spacecraft {people['craft']}")

print('----------------------------------------------------------------Task 2-------------------------------------------------------------------')

"""завдання 2
апі погоди (всі токени я для вас вже прописав)
https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric
де city_name - назва міста на аглійській мові (наприклад, odesa, kyiv, lviv, london)
погода змінюється, як і місто. яке ви введете
роздрукувати тепрературу та швидкість вітру. з вказівкою міста, яке було вибране"""


city_name = input("Weather of which city are you interested in? ")
while True:
    url_weather = f'https://api.openweathermap.org/data/2.5/weather?q={city_name}&appid=47503e85fabbabc93cff28c52398ae97&units=metric'
    answer = requests.get(url_weather)
    answer_json = answer.json()
    cod = answer_json['cod']
    if cod == 200:
        break
    else:
        city_name = input("You have done a mistake in the city name, please, write it again: ")

main_list = answer_json['main']
temp_city = main_list['temp']
wind_list = answer_json['wind']
wind_speed = wind_list['speed']
print(f'The temperature in the {city_name} city is {temp_city}°C and the speed of the wind is {wind_speed} m/sec')
