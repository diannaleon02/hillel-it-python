import copy

class Point:
    _x = None
    _y = None

    def __init__(self, dot_x=0, dot_y=0):
        if (isinstance(dot_x, (int, float))
                and isinstance(dot_y, (int, float))):
            self._x = dot_x
            self._y = dot_y
        else:
            raise TypeError

    @property
    def x(self):
        return self._x

    @x.setter
    def x(self, dot_x):
        if (isinstance(dot_x, (int, float))):
            self._x = dot_x
        else:
            raise TypeError

    @property
    def y(self):
        return self._y

    @y.setter
    def y(self, dot_y):
        if (isinstance(dot_y, (int, float))):
            self._y = dot_y
        else:
            raise TypeError

class Line:
    _begin = None
    _end = None

    def __init__(self, begin_point, end_point):
        self.begin = begin_point
        self.end = end_point

    @property
    def length(self):
        cat1 = self.begin.x - self.end.x
        cat2 = self.begin.y - self.end.y

        res = (cat1 ** 2 + cat2 ** 2) ** 0.5

        return res

    @property
    def begin(self):
        return self._begin

    @begin.setter
    def begin(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self._begin = value

    @property
    def end(self):
        return self._end

    @end.setter
    def end(self, value):
        if not isinstance(value, Point):
            raise TypeError
        self._end = value

class Triangle:
    _tops = []
    _sides = []

    def __init__(self, top_points):
        if(isinstance(top_points, list)):
            for top in top_points:
                if (not isinstance(top, Point)):
                    raise TypeError
        else:
            raise TypeError

        self._tops = copy.deepcopy(top_points)
        _sides = []
        self._sides.append(Line(top_points[0], top_points[1]))
        self._sides.append(Line(top_points[1], top_points[2]))
        self._sides.append(Line(top_points[2], top_points[0]))


    @property
    def tops(self):
        return self._tops

    @tops.setter
    def tops(self, top_points):
            for top in top_points:
                if(not isinstance(top, Point)):
                    raise TypeError
            self._tops = copy.deepcopy(top_points)

    @property
    def sides(self):
        return self._sides

    @property
    def area(self):
        halp_perimitr = 0
        area = 0
        for side in self.sides:
            halp_perimitr += side.length

        halp_perimitr /= 2

        area = (halp_perimitr * (halp_perimitr - self.sides[0].length) * (halp_perimitr - self.sides[1].length) * (halp_perimitr - self.sides[2].length)) ** 0.5

        return area


point_1 = Point(0, 0)
point_2 = Point(2, 0)
point_3 = Point(0, 2)

triangle = Triangle([point_1, point_2, point_3])

print(triangle.area)



