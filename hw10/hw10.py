class Students:
    name = "Tom"
    age = "19"
    study = "default field of study"

    def homework_pass(self):
        print("Give the teacher your homework: ")

    def __init__(self, name, age, study):
        self.name = name
        self.age = age
        self.study = study


class StudentSchool(Students):
    def homework_pass(self):
        print(f"Give the teacher your {self.study} copybook to check your homework: ")


class StudentUniversity(Students):
    def homework_pass(self):
        print(f"Bring your {self.study} assignment to the university teacher to check it and correct: ")


class StudentCourses(Students):
    def homework_pass(self):
        print(f"Send the teacher at online courses your {self.study} homework files via email: ")


student_1 = StudentSchool("Jack",8, "mathematics")
student_1.homework_pass()
student_2 = StudentUniversity("Edward",22, "statistics")
student_2.homework_pass()
student_3 = StudentCourses("Sasha",37, "programming")
student_3.homework_pass()