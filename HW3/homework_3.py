""" 1. Зформуйте строку, яка містить певну інформацію про символ по його індексу в відомому слові.
Наприклад "The [індекс символу] symbol in [тут слово] is '[символ з відповідним порядковим номером]'".
Слово та номер отримайте за допомогою input() або скористайтеся константою.
Наприклад (слово - "Python" а номер символу 3) - "The 3 symbol in "Python" is 't' ". """

print("------------------------------------Solution for the first task: ----------------------------------------")

word = input('Give me a word, please: ')

while len(word) == 0:
    word = input('You have given me the empty line, please, give me a word: ')

number = input('Give me your desired number of the symbol,please: ')

while True:
    if number.isdigit():
        if int(number) <= len(word) and int(number) != 0:
            break
        else:
            number = input('Your number is over the length of your previously given word. Please, give me a number that is not over the lenght of your word: ')
    else:
        number = input('You have given me no digit, please, give a number which is digit: ')

number_2 = int(number)

symbol = word[number_2 - 1]

print(f'The {number_2} symbol in "{word}" is "{symbol}"')


"""2. Вести з консолі строку зі слів за допомогою input() (або скористайтеся константою). 
   Напишіть код, який визначить кількість слів, в цих даних."""

print("------------------------------------1st option to solve the second task: ----------------------------------------")


sentence = input('Give me a sentence, please: ')

while len(sentence) == 0:
    sentence = input('You have given me the empty line, please, give me a sentence: ')

sentence = sentence.split()

sentence = " ".join(sentence)

word_counter = 0

for letter in sentence:
    if letter == " ":
        word_counter += 1

word_counter += 1

print(f'The amount of words is {word_counter}')

print("------------------------------------2nd option to solve the second task: ----------------------------------------")

sentence = input('Give me a sentence, please: ')

while len(sentence) == 0:
    sentence = input('You have given me the empty line, please, give me a sentence: ')

sentence = sentence.split()

print(f'The amount of words is {len(sentence)}')


"""3.Існує ліст з різними даними, наприклад lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
Напишіть код, який сформує новий list (наприклад lst2), який би містив всі числові змінні (int, float), які є в lst1.
Майте на увазі, що данні в lst1 не є статичними можуть змінюватись від запуску до запуску."""

print("------------------------------------Solution for the third task: ----------------------------------------")

list1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']

list2 = []

print(list1)

for item in list1:
    if type(item) is int or type(item) is float:
        list2.append(item)

print(list2)


